import React from "react";
import ReactDOM from "react-dom";

class SideMenu extends React.Component<any, any> {
  // fires before component is mounted
  constructor(props: any) {
    // makes this refer to this component
    super(props);

    // set local state
    this.state = {
      date: new Date(),
      name: "Arun Sharma",
    };
  }
  handleChange = (event: any) => {
    this.setState({ name: event.target.value });
  };
  render() {
    return (
      <div>
        <input
          type="text"
          value={this.state.name}
          onChange={this.handleChange}
        />

        <h1>
          Hi User {this.state.name} logged in at{" "}
          {this.state.date.toLocaleTimeString()}.
        </h1>
      </div>
    );
  }
}

export default SideMenu;
